# Automated Code Review

While [Code Review](../../../../code-review/) is often done by humans, [Automated Code Review](https://en.wikipedia.org/wiki/Automated_code_review) is sending machines to scan the code automatically and alert human code authors about potential areas where their code might not be violating code review guidelines. Using Automated Code Review tools has been a standard development practice in both Open Source and commercial software domains[^1]. At Smarter.Codes we want to make it a standard practice as well - across all software engineering projects.

[^1]: Footnote borrowed from wikipedia: [The impact of code review coverage and code review participation on software quality](https://en.wikipedia.org/wiki/Automated_code_review#cite_ref-1)

# Factors which help us choose the right mix of Automated Code Review solution(s) to add to our CI Pipeline
There are several kinds of automated code review solutions. Some would.. 
1. Return a neat list of warnings
2. Help you visualize your large code, and let you (a human) take the call on whether the ~Review-Design is good enough.
3. Static code analysis for any security issues
4. Inform the developer regarding code quality improvement/reduction
3. enlist more kinds here..

The factors we should use the pick the right tool(s) should be
1. The more the rules are checked. The better
2. Enlist factor 2 here..

